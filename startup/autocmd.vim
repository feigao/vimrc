augroup basic_group
  autocmd!

  " reset window layout when vim resized
  autocmd VimResized * wincmd =

  " enter insert mode when switch to terminal buffer
  autocmd BufWinEnter,WinEnter term://* startinsert
  autocmd BufLeave term://* stopinsert

  " update an open buffer if it has been changed outside
  autocmd CursorHold * silent! checktime

  " remember cursor position
  autocmd BufReadPost *
        \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
        \ |   exe "normal! g`\""
        \ | endif

  " filetype settings
  autocmd filetype vim setlocal foldmethod=marker foldenable shiftwidth=2
  autocmd filetype text,markdown setlocal cole=0 kp=dict\ -d\ wn
  autocmd filetype haskell setlocal fo-=ro sw=2 et sts=2 ts=2 kp=hoogle fdm=indent
  autocmd BufReadPost *.js setlocal ft=javascript
  autocmd filetype html,javascript setlocal sw=2 sts=2 et
  autocmd filetype apache setlocal commentstring=#\ %s
  autocmd filetype neosnippet setlocal noexpandtab ts=4 sw=4
  autocmd filetype make setlocal noexpandtab sw=4
  autocmd filetype lisp let b:AutoPairs = {'(':')', '[':']', '{':'}', '"':'"', '`':'`'}
  autocmd filetype vim let b:AutoPairs = {'(':')', '[':']', '{':'}', "'":"'", '`':'`'}
  autocmd BufNewFile,BufRead CMakeLists.txt setlocal filetype=cmake

  autocmd filetype python nnoremap <buffer> <F5> :w !python<cr>

  " auto source vimrc file after save
  autocmd BufWritePost $MYVIMRC source $MYVIMRC

  " create directories if not exists on file save
  autocmd BufWritePre *
        \ if '<afile>' !~ '^scp:' && !isdirectory(expand('<afile>:h')) |
        \ call mkdir(expand('<afile>:h'), 'p') |
        \ endif
augroup END

augroup NoSimultaneousEdits
  " when opening a file that opened in another session, do not moan
  " just open it in [RO]
  autocmd!
  autocmd SwapExists * let v:swapchoice='o'
  autocmd SwapExists * echo 'Duplicate edit session (readonly)'
  autocmd SwapExists * echohl None
augroup end

" Highlight current line on active window only
augroup line_highlight
    autocmd!
    autocmd WinEnter * setlocal cursorline
    autocmd WinLeave * setlocal nocursorline
augroup END

" Use vertical splits
augroup vertical_help
    autocmd!
    autocmd FileType help wincmd L
augroup END

" Trim whitespapes for specified file types upon write
" augroup trim_white_space
"     autocmd!
"     autocmd FileType vim,c,cpp,css,java,php,javascript,python autocmd BufWritePre <buffer> %s/\s\+$//e
" augroup END
