syntax on
" use plugins
filetype plugin indent on

" Highlight line containing the cursor
set cursorline
" Automatically Read changes made to file outside of Vim
set autoread
set noswapfile
set splitbelow splitright
" Hide buffers with unsaved changes without being prompted
set hidden
" Restrict existing tab to width of 4 spaces
set softtabstop=4
" Use 4 spaces for tabs
set shiftwidth=4
" Always expand tabs to spaces
set expandtab
set autoindent smarttab
set backspace=indent,eol,start
" Show line numders
set number
" show relative line number
set relativenumber
" Enable mouse interaction
set mouse=a
set belloff=all complete-=i display+=lastline
set encoding=utf-8 nrformats-=octal
set formatoptions+=j
set path+=**  " search down into subfolders, provides, tab-completion for all file-related tasks
set timeout timeoutlen=1000  " for mappings
set ttimeout ttimeoutlen=100  " for key codes
" Trigger CursorHold event if nothing is typed for the duration
set updatetime=300
set hlsearch
" Use interactive search
set incsearch
" Default search is not case sensitive
set ignorecase
" Search will be case sensitive if uppercase character is entered
set smartcase
set showcmd noshowmode
set ttyfast laststatus=2
" Auto-complete on tab, while in command mode
set wildmenu
" visual autocomplete for command menu
set wildmode=full
" Number of columns to scroll horizontally when cursor is moved off the screen
set scrolloff=3
" Minimum number of lines to keep before scrolling
set sidescroll=1
" Minimum number of columns to keep towards the right of the cursor
set sidescrolloff=5

set conceallevel=2 concealcursor=niv  " reveal markers
set langnoremap nolangremap
set foldlevel=99  " unfold by default
" Use interactive replace
if exists('&icm') | set inccommand=nosplit | endif
let g:vimsyn_folding='af'

" disable modelines
set nomodeline
" Use English dictionary
set spelllang=en_gb
" Dissable Background Color Erase, filling background with current themes color
set t_ut=
" Ignore case when completing in command mode
set wildignorecase
" Donot use popup menu for completions in command mode
set wildoptions=tagfile
" Auto select the first entry but don't insert
set completeopt=noinsert,menuone,preview
" Stop popup menu messages (like: match 1 of 2, The only match etc.)
set shortmess+=c
" Don't update screen while macro or command/script is executing
set lazyredraw
" Larger command input hight to better display messages
" set cmdheight=2
" Don't show mode as it is already displayrd in status line
set noshowmode
" Always show diagnostics column
set signcolumn=yes
" Max number of items visible in popup menu
set pumheight=12
" Settings for better diffs
set diffopt=filler,vertical,hiddenoff,foldcolumn:0,algorithm:patience
" Show whitespace characters
set list
" Only show tabs and trailing spaces
set listchars=tab:▶-,trail:●,extends:◣,precedes:◢
" set listchars=tab:>-,trail:-,extends:>,precedes:<,nbsp:+ list
" Don't wrap long lines
set nowrap
" Tell neovim where to look for tags file
set tags=/tmp/tags

" Enable True Color support
if (has('termguicolors'))
  set termguicolors
endif
" Undotree settings
if has('persistent_undo')
    set undodir=~/.local/share/nvim/undodir/
    set undofile
endif
" dissable some features when running as Root
if exists('$SUDO_USER')
    set nobackup
    set nowritebackup
    set noundofile
    set viminfo=
endif
" Use ripgrep as the grep program
if executable('rg')
  set grepprg=rg\ --vimgrep\ --no-heading\ --smart-case
  set grepformat=%f:%l:%c:%m,%f:%l:%m
endif

set background=light

""" colorscheme customisation {{{
""" Note: italic not working in tmux

let g:gruvbox_italic=0

let g:nord_underline=1
" let g:nord_italic=1
" let g:nord_italic_comments=1
let g:nord_comment_brightness=15
let g:nord_cursor_line_number_background=1

""" vim-colorschemes
" Use :Colors to select
colorscheme PaperColor
"nord  "gruvbox "onedark "elflord "PaperColor
""" }}}



" " Set up highlighting for trailing whitespace
" highlight Whitespace    guifg=#6272A4   guibg=#282A36
" " configure highlights for wild menu (command mode completions)
" highlight StatusLine    guibg=#3E4452   ctermbg=240
" highlight WildMenu      guifg=#50FA7B   guibg=#3E4452   ctermfg=76     ctermbg=240
