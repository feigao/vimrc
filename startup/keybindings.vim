inoremap kj <esc>

" enhance the system defaults by fzf
imap <c-x><c-f> <plug>(fzf-complete-path)
imap <c-x><c-k> <plug>(fzf-complete-word)

" Use K for show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

" clear highlights
nnoremap <silent> <cr> :nohlsearch<c-r>=has('diff')?'<Bar>diffupdate':''<cr><cr>

" Vmap for maintain Visual Mode after shifting > and <
vnoremap < <gv
vnoremap > >gv
"" Move visual block
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

if exists(':tnoremap')
  " escape terminal by <esc>
  tnoremap <expr> <esc> &filetype == 'fzf' ? "\<c-g>" : "\<C-\>\<C-n>"
endif

" resize current buffer by +/- 5 (shift+arrows)
nnoremap <S-up>    :resize -1<cr>
nnoremap <S-down>  :resize +1<cr>
nnoremap <S-left>  :vertical resize -3<cr>
nnoremap <S-right> :vertical resize +3<cr>

" Window navigation function
" Make ctrl-h/j/k/l move between windows and auto-insert in terminals
func! s:mapMoveToWindowInDirection(direction)
  func! s:maybeInsertMode(direction)
    stopinsert
    execute 'wincmd' a:direction
    if &buftype ==# 'terminal'
      startinsert!
    endif
  endfunc
  execute 'tnoremap' '<silent>' '<C-' . a:direction . '>'
        \ "<C-\\><C-n>"
        \ ":call <SID>maybeInsertMode(\"" . a:direction . "\")<CR>"
  execute 'nnoremap' '<silent>' '<C-' . a:direction . '>'
        \ ":call <SID>maybeInsertMode(\"" . a:direction . "\")<CR>"
endfunc
for dir in ['h', 'j', 'l', 'k']
  call s:mapMoveToWindowInDirection(dir)
endfor

" j/k movement w.r.t visual wrap
nnoremap <expr> j v:count ? (v:count > 5 ? "m'" . v:count : '') . 'j' : 'gj'
nnoremap <expr> k v:count ? (v:count > 5 ? "m'" . v:count : '') . 'k' : 'gk'

" buffers
nnoremap [b :bp<cr>
nnoremap ]b :bn<cr>

nnoremap [ob :set background=dark<cr>
nnoremap ]ob :set background=light<cr>

" change word under cursor and save in dot repeat
nnoremap c* *Ncgn
nnoremap c# #NcgN

" make search always apear in center of page
nnoremap n    nzz
nnoremap N    Nzz
nnoremap *    *zz
nnoremap #    #zz
nnoremap {    {zz
nnoremap }    }zz
nnoremap g*   g*zz
nnoremap g#   g#zz

function! ToggleMovement(firstOp, thenOp) abort
  let pos = getpos('.')
  execute 'normal! ' . a:firstOp
  if pos == getpos('.')
    execute 'normal! ' . a:thenOp
  endif
endfunction

nnoremap <silent> 0 :call ToggleMovement('^', '0')<cr>

""" vim-which-key
set timeoutlen=500
let g:which_key_map = {}

nnoremap <leader>bv :ls<cr>:vert sbuffer<space>
" delete all but current buffer
nnoremap <silent> <leader>bo :w<cr>:%bd<cr>:e#<cr>:bd#<cr>
let g:which_key_map.b = { 'name': '+buffer',
      \ 'b' : ['Buffers'               , 'fzf-buffers']              ,
      \ 'd' : ['bdelete'               , 'delete-buffer']            ,
      \ 'D' : ['BD'                    , 'delete-buffer-non-intact'] ,
      \ 'w' : ['bw'                    , 'wipe-buffer']              ,
      \ 'W' : ['BW'                    , 'wipe-buffer-nonintact']    ,
      \ 'f' : ['bfirst'                , 'first-buffer']             ,
      \ 'l' : ['blast'                 , 'last-buffer']              ,
      \ 'n' : ['bnext'                 , 'next-buffer']              ,
      \ 'p' : ['bprevious'             , 'previous-buffer']          ,
      \ 'v' : 'open-buffer-vertical'   ,
      \ 'o' : 'kill-all-other-buffers'
      \ }

nnoremap <silent> <leader>fd :e $MYVIMRC<cr>
nnoremap <silent> <leader>f. :lcd %:p:h<cr>
nnoremap <silent> <leader>f= :Neoformat<cr>:w<cr>
let g:which_key_map['f'] = { 'name' : '+file',
      \ 'f' : ['Files'           , 'fzf-search-files'] ,
      \ 'r' : ['History'         , 'recent-opened']    ,
      \ 's' : ['update'          , 'save-file']        ,
      \ '.' : 'lcd-current-path' ,
      \ 'd' : 'open-vimrc'       ,
      \ '=' : 'neoformat-file'   ,
      \ }

let g:which_key_map.l = { 'name' : '+lsp',
      \ 'f' : ['LanguageClient#textDocument_formatting()'     , 'formatting']       ,
      \ 'h' : ['LanguageClient#textDocument_hover()'          , 'hover']            ,
      \ 'r' : ['LanguageClient#textDocument_references()'     , 'references']       ,
      \ 'R' : ['LanguageClient#textDocument_rename()'         , 'rename']           ,
      \ 's' : ['LanguageClient#textDocument_documentSymbol()' , 'document-symbol']  ,
      \ 'S' : ['LanguageClient#workspace_symbol()'            , 'workspace-symbol'] ,
      \ 'm' : ['LanguageClient_contextMenu()'                 , 'context-menu']     ,
      \ 'd' : ['LanguageClient#textDocument_definition()'     , 'definition']       ,
      \ 't' : ['LanguageClient#textDocument_typeDefinition()' , 'type-definition']  ,
      \ 'i' : ['LanguageClient#textDocument_implementation()' , 'implementation']   ,
      \ }

""" tabular
" vnoremap <leader>t, :Tabularize /^[^,]*\zs,/l0c1<CR>
vnoremap <leader>t, :Tabularize /,/r0c1<CR>
vnoremap <leader>t: :Tabularize /^[^:]*\zs:/l1l1<CR>
vnoremap <leader>t= :Tabularize /^[^=]*\zs=/l1c1<CR>
vnoremap <leader>t<space> :Tabularize /\S\+/l1l0<CR>
let g:which_key_map.t = { 'name': '+tabular',
      \ ',' : 'align first ,',
      \ ':' : 'align first :',
      \ '=' : 'align first =',
      \ ' ' : 'align space',
      \ 'a' : ['<Plug>(EasyAlign)', 'easy-align'],
      \ }

""" denite
nnoremap <silent> <leader>xx :Denite command<cr>
let g:which_key_map.x = {'name' : '+denite',
      \ 'x' : 'denite-command',
      \ }

" let g:which_key_map.w = {'name' : '+vimwiki' }

" copy/paste with system clipboard
nnoremap <leader>y "+y
vnoremap <leader>y "+y
nnoremap <leader>d "+d
vnoremap <leader>d "+d
nnoremap <leader>p :set paste<cr>"+gp:set nopaste<cr>
vnoremap <leader>p :set paste<cr>"+gp:set nopaste<cr>
nnoremap <leader>P :set paste<cr>"+gP:set nopaste<cr>
vnoremap <leader>P :set paste<cr>"+gP:set nopaste<cr>
let g:which_key_map.y = 'yank-to-system-clipboard'
let g:which_key_map.d = 'kill-to-system-clipboard'
let g:which_key_map.p = 'paste-after-from-system-clipboard'
let g:which_key_map.P = 'paste-before-from-system-clipboard'

""" vim-gitgutter
" jump to next hunk (change): ]c
" jump to previous hunk (change): [c.
" stage the hunk with <Leader>hs or
" undo it with <Leader>hu.
" preview a hunk's changes with <Leader>hp
let g:which_key_map.g = {
      \ 'name' : '+git' ,
      \ 's' : 'stage-hunk' ,
      \ 'u' : 'unstage-hunk' ,
      \ 'p' : 'preview-hunk-changes',
      \ }

call which_key#register('<Space>', 'g:which_key_map')
nnoremap <silent> <leader> :<c-u>WhichKey '<Space>'<cr>
vnoremap <silent> <leader> :<c-u>WhichKeyVisual '<Space>'<cr>
nnoremap <localleader> :<c-u>WhichKey  ','<CR>
vnoremap <localleader> :<c-u>WhichKeyVisual  ','<CR>
