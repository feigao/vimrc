""" fzf
" use :FZF to search, then c-x/c-v/c-t to split/vsplit/tab
" add :Rg command for searching
command! -bang -nargs=* Rg
      \ call fzf#vim#grep(
      \   'rg --column --line-number --no-heading --color=always --smart-case '.shellescape(<q-args>), 1,
      \   <bang>0 ? fzf#vim#with_preview('up:60%')
      \           : fzf#vim#with_preview('right:50%:hidden', '?'),
      \   <bang>0)

" use :Snippets to search snippets with description,
" note: original only search snippet names
command! -bar -bang Snippets call fzf#vim#snippets({'options': '-n ..'}, <bang>0)
