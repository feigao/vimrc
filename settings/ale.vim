""" ale
" let g:ale_enabled = 1  " use :ALEToggle to activate
" let g:ale_set_highlights = 0
" For all languages unspecified in the dictionary, all possible linters will be run for those languages
let g:ale_fixers = {
      \ '*': ['remove_trailing_lines', 'trim_whitespace'] }
" \ 'haskell': ['hfmt', 'trim_whitespace'],
" \ 'python': ['black', 'trim_whitespace'],
let g:ale_fix_on_save = 1
let g:ale_python_flake8_options = '--ignore=E501'
" ale goto previous/next error
nmap <silent> [e <Plug>(ale_previous_wrap)
nmap <silent> ]e <Plug>(ale_next_wrap)
