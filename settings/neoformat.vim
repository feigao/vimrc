""" neoformat
" autocmd pgp BufWritePre * try | undojoin | Neoformat | catch /^Vim\%((\a\+)\)\=:E790/ | finally | silent Neoformat | endtry
let g:neoformat_run_all_formatters = 1
let g:neoformat_basic_format_align = 1
let g:neoformat_basic_format_retab = 1
let g:neoformat_basic_format_trim = 1
let g:neoformat_enabled_haskell = ['brittany', 'stylish-haskell', 'hfmt']
let g:neoformat_enabled_python = ['black']
let g:neoformat_enabled_fsharp = ['fantomas']
