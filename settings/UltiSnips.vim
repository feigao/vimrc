""" ultisnips
let g:UltiSnipsSnippetsDir='~/.config/nvim/UltiSnips'
let g:UltiSnipsUsePythonVersion=3
let g:UltiSnipsEnableSnipMate=1
let g:UltiSnipsEditSplit='horizontal'
let g:UltiSnipsExpandTrigger='<tab>'
let g:UltiSnipsJumpForwardTrigger='<tab>'  "default: <c-j>
let g:UltiSnipsJumpBackwardTrigger='<s-tab>'  "default: <c-k>
