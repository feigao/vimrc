" map <C-n> :NERDTreeToggle<CR>
"
" " open NERDTree automatically when vim starts up on opening a directory
" autocmd pgp StdinReadPre * let s:std_in=1
" autocmd pgp VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif

" let NERDTreeShowHidden=1

" Netrw setup to open (if active and envoked) to the side like NerdTree
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25
" Dissable netrw plugins
let g:loaded_netrw = 1
let g:loaded_netrwPlugin = 1
let g:loaded_netrwSettings = 1
let g:loaded_netrwFileHandlers = 1

" Set NERDTree to hijack netrw
let NERDTreeHijackNetrw=1
