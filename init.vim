let g:mapleader="\<Space>"
let g:maplocalleader=','
let g:python3_host_prog = expand('~/.pyenv/versions/miniconda3-latest/envs/misc@3.7/bin/python3')
let g:loaded_python_provider = 1

""""""""""""""""""""""""""""""
""" vim_plug
""""""""""""""""""""""""""""""
let vimplug_exists=expand('~/.local/share/nvim/site/autoload/plug.vim')
if !filereadable(vimplug_exists)
  silent !curl -fLo "~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
  autocmd VimEnter * PlugInstall
endif

if $USER !=? 'root'
  call plug#begin(expand('~/.local/share/nvim/plugged'))

  Plug 'Shougo/denite.nvim', {'do': ':UpdateRemotePlugins'}  " auto-completion
  Plug 'jiangmiao/auto-pairs'
  Plug 'liuchengxu/vim-which-key'
  Plug 'luochen1990/rainbow'
  Plug 'qpkorr/vim-bufkill'  " kill buffer without closing the window or split
  Plug 'tpope/vim-apathy'  " Set the 'path' option for miscellaneous file types
  Plug 'tpope/vim-commentary'  " comment lines
  Plug 'tpope/vim-fugitive'
  Plug 'tpope/vim-repeat'  " enable repeating suppored plugin maps with .
  Plug 'tpope/vim-rsi'  " Readline mappings in insert mode and command line mode
  Plug 'tpope/vim-sleuth'  " automatically adjusts shiftwidth and expandtab intelligently
  Plug 'tpope/vim-surround'   " surroundings
  Plug 'vim-airline/vim-airline'
  Plug 'SirVer/ultisnips'
  Plug 'honza/vim-snippets'
  if isdirectory('/usr/local/opt/fzf')
    Plug '/usr/local/opt/fzf'
    Plug 'junegunn/fzf.vim'
  else
    Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --bin' }
    Plug 'junegunn/fzf.vim'
  endif
  Plug '907th/vim-auto-save'
  Plug 'airblade/vim-gitgutter'  " show git diff in gutter and stage/undo hunks
  Plug 'andymass/vim-matchup'
  Plug 'christoomey/vim-tmux-navigator'
  Plug 'godlygeek/tabular' , {'on': 'Tabularize'}
  Plug 'junegunn/vim-easy-align'
  Plug 'michaeljsmith/vim-indent-object'  " indent object: dii/dai/diI/daI
  Plug 'rizzatti/dash.vim'    " use :Dash to search in Dash.app
  Plug 'sbdchd/neoformat'     " code formatting
  Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
  " Plug 'Xuyuanp/nerdtree-git-plugin'
  Plug 'wellle/targets.vim'   " provide additional text objects

  " programming
  Plug 'autozimu/LanguageClient-neovim', {'branch': 'next', 'do': 'bash install.sh'}
  Plug 'w0rp/ale'             " async lint
  if has('nvim')
    Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
  else
    Plug 'Shougo/deoplete.nvim'
    Plug 'roxma/nvim-yarp'
    Plug 'roxma/vim-hub-neovim-rpc'
  endif

  " filetypes
  Plug 'jceb/vim-orgmode'
  Plug 'sheerun/vim-polyglot' " syntax highlight and indent
  Plug 'fsharp/vim-fsharp', {'for': 'fsharp', 'do': 'make fsautocomplete'}
  Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'}  " semantic highlight for python in neovim
  Plug 'eagletmt/neco-ghc'  " ghc-mod completion for deoplete
  Plug 'elmcast/elm-vim'
  Plug 'pbrisbin/vim-syntax-shakespeare'

  " colorscheme
  Plug 'flazz/vim-colorschemes'

  call plug#end()

  """ PLUGIN SETTINGS
  " Configure plugins, plugin specific functions and autocommands are to be
  " written in the corresponding files (makes debuging and trying out
  " plugins easier)
  source ~/.config/nvim/settings/NERDTree.vim
  source ~/.config/nvim/settings/languageclient-neovim.vim
  source ~/.config/nvim/settings/UltiSnips.vim
  source ~/.config/nvim/settings/fzf.vim
  source ~/.config/nvim/settings/neoformat.vim
  source ~/.config/nvim/settings/vim-airline.vim
  source ~/.config/nvim/settings/deoplete.vim
  source ~/.config/nvim/settings/ale.vim

endif " $USER != "root"

source ~/.config/nvim/startup/options.vim
source ~/.config/nvim/startup/autocmd.vim
source ~/.config/nvim/startup/functions.vim
source ~/.config/nvim/startup/keybindings.vim

""" Plug '907th/vim-auto-save'
let g:auto_save        = 1
let g:auto_save_silent = 1
let g:auto_save_events = ['InsertLeave', 'TextChanged', 'FocusLost']

""" Plug 'sheerun/vim-polyglot'
let g:polyglot_disabled = ['elm']

""" 'fsharp/vim-fsharp'
let fsharp_map_keys=0
